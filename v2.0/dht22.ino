/*
 * Definiciones del sensor dht22
 * huertechno.cf
 */
#define DHTPIN D2  //DHT 
#define DHTTYPE DHT22  //DHT


DHT dht(DHTPIN, DHTTYPE); 

void dht_setup(){
  pinMode(DHTPIN, INPUT);
}


void dht_loop(){
  char TEM_measure[6]= "";
  char HUM_measure[6]= "";
  dht_med dht_measure = dht_sensor();
  float tem_med = dht_measure.tem;
  float hum_med = dht_measure.hum;

  sprintf(TEM_measure, "%.02f", tem_med); //convertir a char*
  pub("1/SENSOR/1/TEM", TEM_measure);
  sprintf(HUM_measure, "%.02f", hum_med); //convertir a char*
  pub("1/SENSOR/2/HUM", HUM_measure);
  
}


dht_med dht_sensor(){
  dht_med measure; 
  measure.hum = dht.readHumidity();
  measure.tem = dht.readTemperature();
  if (isnan(measure.hum) || isnan(measure.tem)){
    Serial.println();
    Serial.print("Error DHT:");
    Serial.println();    
  }
  return measure;
}
