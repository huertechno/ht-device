/*
 * Definiciones del componente yl69
 */
#define HUM_SOIL_PIN A0

void yl69_loop(){
  char HUM_SOIL_measure[6]= "";
  float hum_soil = yl69_sensor();
  sprintf(HUM_SOIL_measure, "%.02f", hum_soil);
  pub("1/SENSOR/3/HUM_SOIL", HUM_SOIL_measure);
}

float yl69_sensor(){
  float measure; //define struct dht_med
  measure = map(analogRead(HUM_SOIL_PIN), 400, 1024, 99, 0);
  if (isnan(measure)){
    Serial.println();
    Serial.print("Error YL69:");
    Serial.println();    
  }
  return measure;
}
