/*
 * definiciones de red
 */

void red_setup(){
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
}

void red_loop(){
  if (!client.connected()) {
    reconnect();
  }
  client.loop();
}


void setup_wifi() {
  delay(10);
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}


void reconnect() {
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    if (client.connect("LaHuerta1")) {
      // se conecta con el clientid, usuario y passqord   
      Serial.println("connected");
      client.subscribe(topic_sub);
      
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      delay(5000);
    }
  }
}

void pub(char *topico, char *msg){
  
  //Serial.print("Mensaje publicado: ");
  //Serial.print(topico);
  //Serial.print(":");
  //Serial.println(msg);
  client.publish(topico, msg);

}

void callback(char* topic, byte* payload, unsigned int length) {
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  
  Serial.println("");
  String functionMqtt = String(topic);
  Serial.println(String(topic));
  if (functionMqtt == topic){
    accion_component(payload);
  }
  
  Serial.println();
}


int accion_component(byte* payload){
  Serial.print("----Accion:");
  int ACT_PIN = RELE_PIN[payload[0] - 53] ;
  Serial.print("ACT_PIN:");
  Serial.println(ACT_PIN);
  int state = payload[2] - 48 ;
  Serial.print("State: ");
  Serial.println(state);
  digitalWrite(ACT_PIN, state); 
  return 0; 
 }
