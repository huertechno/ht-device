#include <Arduino.h>
#include "Thing.h"
#include "WebThingAdapter.h"
#include <DHT.h>


//#define DHTPIN D2  //DHT 
#define DHTTYPE DHT22  //DHT

DHT dht(4, DHTTYPE); 
int TIME_DELAY=500;

//TODO: Hardcode your wifi credentials here (and keep it private)
const char* ssid = "... tu wifi";
const char* password = "tu pass";

//pin default, el de color azul
#if defined(LED_BUILTIN)
const int ledPin = LED_BUILTIN;
#else
const int ledPin = 13;  // manully configure LED pin
#endif

//se define un adaptador Ej: nodemcu
WebThingAdapter* adapter = NULL;

const char* deviceTypes[] = {"MultiLevelSensor", "Sensor", nullptr};
//humedad de suelo
ThingDevice yl69("yl69", "Sensor de humedad de tierra", deviceTypes);
ThingProperty yl69Level("level", "Analog Input pin", NUMBER, "LevelProperty");

//temperatura

ThingDevice dht22t("dht22T", "Sensor de temperatura", deviceTypes);
ThingProperty dht22tLevel("level", "Analog Input pin", NUMBER, "LevelProperty");

//humedad ambiente
ThingDevice dht22h("dht22H", "Sensor de humedad", deviceTypes);
ThingProperty dht22hLevel("level", "Analog Input pin", NUMBER, "LevelProperty");


//relays
const char* relayTypes[] = {"OnOffSwitch", "Relay", nullptr};
ThingDevice relay1("r1", "Luz led", relayTypes);
ThingProperty relay1On("on", "", BOOLEAN, "OnOffProperty");

ThingDevice relay2("r2", "Regador 1", relayTypes);
ThingProperty relay2On("on", "", BOOLEAN, "OnOffProperty");

ThingDevice relay3("r3", "Intractor", relayTypes);
ThingProperty relay3On("on", "", BOOLEAN, "OnOffProperty");

ThingDevice relay4("r4", "Extractor", relayTypes);
ThingProperty relay4On("on", "", BOOLEAN, "OnOffProperty");




//pines
const int sensorPin = A0;
const int dht22Pin = 4;
const int relay1Pin = 14;
const int relay2Pin = 12;
const int relay3Pin = 13;
const int relay4Pin = 15;

//last values
double lastValueYl69 = 0;
double lastValueDht22t = 0;
double lastValueDht22h = 0;
bool lastLedOn = false;
bool relay1LastOn = false;
bool relay2LastOn  = false;
bool relay3LastOn  = false;
bool relay4LastOn  = false;



void setup(void){
  pinMode(ledPin, OUTPUT);
  pinMode(relay1Pin , OUTPUT);
  pinMode(relay2Pin , OUTPUT);
  pinMode(relay3Pin , OUTPUT);
  pinMode(relay4Pin , OUTPUT);
  pinMode(dht22Pin , INPUT);
  digitalWrite(ledPin, HIGH);
  digitalWrite(relay1Pin , HIGH);
  digitalWrite(relay2Pin , HIGH);
  digitalWrite(relay3Pin , HIGH);
  digitalWrite(relay4Pin , HIGH);
  
  Serial.begin(115200);
  Serial.println("");
  Serial.print("Connecting to \"");
  Serial.print(ssid);
  Serial.println("\"");
#if defined(ESP8266) || defined(ESP32)
  WiFi.mode(WIFI_STA);
#endif
  WiFi.begin(ssid, password);
  Serial.println("");

  // Wait for connection
  bool blink = true;
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
    digitalWrite(ledPin, blink ? LOW : HIGH); // active low led
    blink = !blink;
  }
  digitalWrite(ledPin, HIGH); // active low led

  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
  
  adapter = new WebThingAdapter("analog-sensor", WiFi.localIP());
  //agregar device y property
  yl69.addProperty(&yl69Level);
  adapter->addDevice(&yl69);

  dht22t.addProperty(&dht22tLevel);
  adapter->addDevice(&dht22t);

  dht22h.addProperty(&dht22hLevel);
  adapter->addDevice(&dht22h);

  relay1.addProperty(&relay1On);
  adapter->addDevice(&relay1);

  relay2.addProperty(&relay2On);
  adapter->addDevice(&relay2);  

  relay3.addProperty(&relay3On);
  adapter->addDevice(&relay3);

  relay4.addProperty(&relay4On);
  adapter->addDevice(&relay4);
  
  adapter->begin();
  Serial.println("HTTP server started");
  Serial.print("http://");
  Serial.print(WiFi.localIP());
  Serial.print("/things/");
  Serial.println(yl69.id);

  Serial.print("http://");
  Serial.print(WiFi.localIP());
  Serial.print("/things/");
  Serial.println(dht22t.id);

  Serial.print("http://");
  Serial.print(WiFi.localIP());
  Serial.print("/things/");
  Serial.println(dht22h.id);

  Serial.print("http://");
  Serial.print(WiFi.localIP());
  Serial.print("/things/");
  Serial.println(relay1.id);

  Serial.print("http://");
  Serial.print(WiFi.localIP());
  Serial.print("/things/");
  Serial.println(relay2.id);

    Serial.print("http://");
  Serial.print(WiFi.localIP());
  Serial.print("/things/");
  Serial.println(relay3.id);

    Serial.print("http://");
  Serial.print(WiFi.localIP());
  Serial.print("/things/");
  Serial.println(relay4.id);
}

void loop(void){
  const int threshold = 1;
  int value = analogRead(sensorPin);
  double percent = (double) 100. - (value/1204.*100.);
  if (abs(percent - lastValueYl69) >= threshold) {
    Serial.print("log: Value: ");
    Serial.print(value);
    Serial.print(" = ");
    Serial.print(percent);
    Serial.println("%");
    ThingPropertyValue levelYl69;
    levelYl69.number = percent;
    yl69Level.setValue(levelYl69);
    lastValueYl69 = percent;
  }
  //temperatura y humedad
  float valHumedad = dht.readHumidity();
  float valTemperatura = dht.readTemperature();
  if (isnan(valHumedad) || isnan(valTemperatura)){
    Serial.println();
    Serial.print("Error DHT:");
    Serial.println();    
  }else {
    
    ThingPropertyValue levelDht22t;
    levelDht22t.number = valTemperatura;
    dht22tLevel.setValue(levelDht22t);
    lastValueDht22t = percent;
    
    Serial.print("log: Value dht22t: ");
    Serial.print(valTemperatura);
    Serial.println("%");


    ThingPropertyValue levelDht22h;
    levelDht22h.number = valHumedad;
    dht22hLevel.setValue(levelDht22h);
    lastValueDht22h = valHumedad;
    
    Serial.print("log: Value dht22t: ");
    Serial.print(valHumedad);
    Serial.println("%");
        
    }

   bool on1 = relay1On.getValue().boolean;
   digitalWrite(relay1Pin, on1 ? LOW : HIGH); // active low led
  if (on1 != relay1LastOn) {
    Serial.print(relay1.id);
    Serial.print(": ");
    Serial.println(on1);
  }
  relay1LastOn = on1; 
  

   bool on2 = relay2On.getValue().boolean;
   digitalWrite(relay2Pin, on2 ? LOW : HIGH); // active low led
  if (on2 != relay2LastOn) {
    Serial.print(relay2.id);
    Serial.print(": ");
    Serial.println(on2);
  }
  relay2LastOn = on2; 


   bool on3 = relay3On.getValue().boolean;
   digitalWrite(relay3Pin, on3 ? LOW : HIGH); // active low led
  if (on3 != relay3LastOn) {
    Serial.print(relay3.id);
    Serial.print(": ");
    Serial.println(on3);
  }
  relay3LastOn = on3; 


   bool on4 = relay4On.getValue().boolean;
   digitalWrite(relay4Pin, on4 ? LOW : HIGH); // active low led
  if (on4 != relay4LastOn) {
    Serial.print(relay4.id);
    Serial.print(": ");
    Serial.println(on4);
  }
  relay4LastOn = on4; 
  
  
  
  
  
  adapter->update();
  delay(TIME_DELAY);
}
